import React, { useEffect, useRef } from "react";
import { NavigationContainer } from "@react-navigation/native";
import AuthLoading from "./src/AuthLoading";
import UserProvider from "./src/context/user";
import { Provider } from "react-redux";
import { store, persistor } from "./src/utils/Store";
import { PersistGate } from "redux-persist/integration/react";
import { NativeBaseProvider } from "native-base";
import {
  useFonts,
  Montserrat_400Regular,
  Montserrat_500Medium,
  Montserrat_600SemiBold,
  Montserrat_700Bold,
  Montserrat_800ExtraBold,
  Montserrat_900Black,
} from "@expo-google-fonts/montserrat";
import { StyleSheet, View, Text } from "react-native";
import { theme } from "./src/styles/theme";
import LottieView from "lottie-react-native";
import { colors } from "./src/styles/colors";

export default function App() {
  let [fontsLoaded] = useFonts({
    Montserrat_400Regular,
    Montserrat_500Medium,
    Montserrat_600SemiBold,
    Montserrat_700Bold,
    Montserrat_800ExtraBold,
    Montserrat_900Black,
  });

  const animation = useRef(null);

  useEffect(() => {
    animation.current.play();
  }, []);

  if (!fontsLoaded) {
    return (
      <View style={styles.loading}>
        <LottieView
          ref={animation}
          autoPlay
          loop
          style={{
            height: 50,
          }}
          source={require("./src/assets/loader.json")}
        />
        <Text style={styles.title}>Tripify</Text>
      </View>
    );
  } else {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <UserProvider>
            <NativeBaseProvider theme={theme}>
              <NavigationContainer>
                <AuthLoading />
              </NavigationContainer>
            </NativeBaseProvider>
          </UserProvider>
        </PersistGate>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  title: {
    fontSize: 36,
    color: colors.primaryText,
    marginTop: 10,
  },
  loading: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
