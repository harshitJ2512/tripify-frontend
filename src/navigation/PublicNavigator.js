import React from "react";
import Onboarding1 from "../screens/Onboarding/Onboarding1";
import Onboarding2 from "../screens/Onboarding/Onboarding2";
import Onboarding3 from "../screens/Onboarding/Onboarding3";
import Login from "../screens/login/Login";
import Otp from "../screens/otp/Otp";
import { createStackNavigator } from "@react-navigation/stack";

const Stack = createStackNavigator();

export default function PublicNavigator() {
  return (
    <Stack.Navigator initialRouteName="onb1">
      <Stack.Screen
        name="onb1"
        component={Onboarding1}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="onb2"
        component={Onboarding2}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="onb3"
        component={Onboarding3}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="login"
        component={Login}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="otp"
        component={Otp}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
}
