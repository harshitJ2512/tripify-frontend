import React from "react";
import Dashboard from "../screens/Dashboard/Dashboard";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { AntDesign } from "@expo/vector-icons";
import CreateTrip from "../screens/CreateTrip/CreateTrip";
import { colors } from "../styles/colors";
import { getFocusedRouteNameFromRoute } from "@react-navigation/native";
import Settings from "../screens/settings/Settings";
const Tab = createBottomTabNavigator();

export default function BottomTabNavigator({ route }) {
  // // const navigation = useNavigation();
  // console.log("focuses route ", getFocusedRouteNameFromRoute(route));
  // // const checkRoute=(routeName) =>
  const checkRoute = () => {
    const res = getFocusedRouteNameFromRoute(route);
    // console.log("focued route", res);
    return res;
  };
  return (
    <Tab.Navigator
      initialRouteName="Dashboard"
      scr
      tabBarOptions={{
        showLabel: false,

        style: {
          height: 70,
          alignItems: "center",
          borderRadius: 30,
          position: "absolute",
          bottom: 30,
          marginHorizontal: 16,
          borderTopColor: "white",
          shadowColor: "#333333",
          shadowOffset: {
            width: 0,
            height: 1,
          },
          // display: "none",
          transform:
            checkRoute() === "Create" ? [{ scale: 0 }] : [{ scale: 1 }],
          shadowOpacity: 0.16,
          shadowRadius: 30,
          paddingBottom: 0,
        },
      }}
    >
      <Tab.Screen
        name="Dashboard"
        component={Dashboard}
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <AntDesign
              name="home"
              size={35}
              color={focused ? "#5864D2" : colors.primaryText}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Create"
        component={CreateTrip}
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <AntDesign
              name="pluscircle"
              size={52}
              color={focused ? "#5864D2" : colors.primaryText}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Settings"
        component={Settings}
        options={{
          tabBarIcon: ({ focused, color, size }) => (
            <AntDesign
              name="setting"
              size={35}
              color={focused ? "#5864D2" : colors.primaryText}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
}
