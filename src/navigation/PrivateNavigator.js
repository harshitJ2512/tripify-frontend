import React from "react";
import BottomTabNavigator from "./BottomTabNavigator";
import { createStackNavigator } from "@react-navigation/stack";
import TripDetails from "../screens/TripDetails/TripDetails";

const Stack = createStackNavigator();

export default function PrivateNavigator() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Bottom"
        component={BottomTabNavigator}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Details"
        component={TripDetails}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
}
