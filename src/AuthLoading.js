import React, { useEffect, useState, useCallback, useContext } from "react";
import PublicNavigator from "./navigation/PublicNavigator";
import PrivateNavigator from "./navigation/PrivateNavigator";
import { createStackNavigator } from "@react-navigation/stack";
import { userContext } from "./context/user";

const Stack = createStackNavigator();

const AuthLoading = () => {
  const [loading, setLoading] = useState(true);
  const { authenticated } = useContext(userContext);
  return (
    <Stack.Navigator initialRouteName="Private">
      {authenticated ? (
        <Stack.Screen
          name="Private"
          component={PrivateNavigator}
          options={{
            headerShown: false,
          }}
        />
      ) : (
        <Stack.Screen
          name="Public"
          component={PublicNavigator}
          options={{
            headerShown: false,
          }}
        />
      )}
    </Stack.Navigator>
  );
};

export default AuthLoading;
