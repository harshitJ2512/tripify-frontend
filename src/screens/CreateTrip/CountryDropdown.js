import React, { useState } from "react";
import { View, StyleSheet, Picker } from "react-native";
// import { Picker } from "@react-native-picker/picker";
import { Entypo } from "@expo/vector-icons";
import { Select, CheckIcon } from "native-base";
const CountryDropdown = () => {
  const [country, setCountry] = useState("");
  return (
    <View style={styles.inputBlock}>
      <Select
        selectedValue={country}
        minWidth={"84%"}
        placeholder="Country"
        // style={styles.input}
        onValueChange={(itemValue) => setCountry(itemValue)}
        variant="plain_select"
        dropdownIcon={
          <Entypo
            // style={styles.location_icon}
            name="location-pin"
            size={25}
            color="#5864D2"
          />
        }
        _selectedItem={{
          bg: "#5864D2",
          endIcon: <CheckIcon size={4} />,
        }}
      >
        <Select.Item label="India" value="India" />
        <Select.Item label="North America" value="North America" />
        <Select.Item label="France" value="France" />
        <Select.Item label="Japan" value="Japan" />
        <Select.Item label="China" value="China" />
      </Select>
    </View>
  );
};

const styles = StyleSheet.create({
  location_icon: {
    position: "absolute",
    top: 18,
    right: 48,
  },
  inputBlock: {
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 4,
    // backgroundColor: "red",
    marginTop: 35,
  },
  input: {
    width: "84%",
    paddingVertical: 5,
    borderBottomWidth: 2,
  },
});

export default CountryDropdown;
