import React, { useState } from "react";
import { View, StyleSheet, Picker } from "react-native";
// import { Picker } from "@react-native-picker/picker";
import { Entypo } from "@expo/vector-icons";
import { Select, CheckIcon } from "native-base";
const CityDropdown = () => {
  const [country, setCountry] = useState("");
  return (
    <View style={styles.inputBlock}>
      <Select
        selectedValue={country}
        minWidth={"84%"}
        placeholder="City"
        onValueChange={(itemValue) => setCountry(itemValue)}
        variant="plain_select"
        dropdownIcon={<Entypo name="location-pin" size={25} color="#5864D2" />}
        _selectedItem={{
          bg: "#5864D2",
          endIcon: <CheckIcon size={4} />,
        }}
      >
        <Select.Item label="Paris" value="Paris" />
        <Select.Item label="Delhi" value="Delhi" />
        <Select.Item label="Chennai" value="Chennai" />
        <Select.Item label="Tokyo" value="Tokyo" />
        <Select.Item label="Las Vegas" value="Las Vegas" />
      </Select>
    </View>
  );
};

const styles = StyleSheet.create({
  inputBlock: {
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 4,
    // backgroundColor: "red",
    marginTop: 35,
  },
  input: {
    width: "84%",
    paddingVertical: 5,
    borderBottomWidth: 2,
  },
});

export default CityDropdown;
