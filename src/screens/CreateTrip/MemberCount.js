import React, { useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import { AntDesign } from "@expo/vector-icons";
import { mediumParagraph } from "../../styles/typography";
const MemberCount = () => {
  const [count, setCount] = useState(0);
  const increment = () => {
    setCount((x) => x + 1);
  };
  const decrement = () => {
    if (count > 0) setCount((x) => x - 1);
  };
  return (
    <View
      style={{
        alignItems: "center",
      }}
    >
      <View style={styles.inputBlock}>
        <Text
          style={{
            ...mediumParagraph,
            fontWeight: "bold",
            fontSize: 18,
            color: "black",
          }}
        >
          Members
        </Text>
        <View style={styles.counter}>
          <AntDesign
            onPress={increment}
            name="pluscircle"
            size={24}
            color="#5864D2"
          />
          <Text style={styles.count}>{count}</Text>
          <AntDesign
            onPress={decrement}
            name="minuscircle"
            size={24}
            color="#5864D2"
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  inputBlock: {
    width: "84%",
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 4,
    justifyContent: "space-between",
    // backgroundColor: "red",
    marginTop: 35,
    flexDirection: "row",
  },
  counter: {
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
  count: {
    ...mediumParagraph,
    marginHorizontal: 10,
    fontSize: 17,
    fontWeight: "800",
  },
});

export default MemberCount;
