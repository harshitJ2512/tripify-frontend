import React from "react";
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  Platform,
  StatusBar,
  TextInput,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Touchable,
} from "react-native";
import CountryDropdown from "./CountryDropdown";
import CityDropdown from "./CityDropdown";
import ActionButton from "../../components/ActionButton";
import MemberCount from "./MemberCount";
import { header } from "../../styles/typography";
import { useToast } from "native-base";
import { FontAwesome5 } from "@expo/vector-icons";
import IternaryDates from "./ItenaryDates";
import { useNavigation } from "@react-navigation/native";
import { AntDesign } from "@expo/vector-icons";
const toast_id = "trip_toast";
const CreateTrip = () => {
  const navigation = useNavigation();
  const toast = useToast();
  const addTrip = () => {
    if (!toast.isActive(toast_id)) {
      toast.show({
        toast_id,
        title: "Trip Created",
        status: "success",
        description: "Your trip has been created succesfully",
      });
    }
    navigation.goBack();
  };
  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
      >
        <View style={styles.heading}>
          <TouchableWithoutFeedback onPress={() => navigation.goBack()}>
            <AntDesign
              style={{ marginLeft: 19 }}
              name="arrowleft"
              size={30}
              color="#5864D2"
            />
          </TouchableWithoutFeedback>
          <Text style={styles.headingText}>Plan Your Adventure</Text>
        </View>
        <CountryDropdown />
        <CityDropdown />

        <IternaryDates />
        <View style={styles.inputBlock}>
          <TextInput style={styles.input} placeholder="Pool Amount" />
          <FontAwesome5
            style={styles.location_icon}
            name="rupee-sign"
            size={24}
            color="#5864D2"
          />
        </View>
        <MemberCount />
        <View style={styles.inputBlock}>
          <ActionButton
            loading={false}
            text="Create Trip"
            onClick={() => addTrip()}
            customStyle={{ width: 350 }}
          />
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    // paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    flex: 1,
    marginTop: 15,
  },
  heading: {
    flexDirection: "row",
    marginTop: 5,
    alignItems: "center",
  },
  headingText: {
    ...header,
    textAlign: "center",
    color: "black",
    fontSize: 27,
    flex: 3,
    marginRight: 17,
    // textDecorationLine: "underline",
  },
  location_icon: {
    position: "absolute",
    top: 15,
    right: 35,
  },
  inputBlock: {
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 4,
    marginTop: 35,
  },
  input: {
    width: "84%",
    paddingVertical: 5,
    borderBottomWidth: 2,
  },
});

export default CreateTrip;
