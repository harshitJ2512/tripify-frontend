import React from "react";
import { View, StyleSheet, Text } from "react-native";
import DateInput from "./DateInput";

const ItenaryDates = () => {
  return (
    <View style={[styles.inputBlock]}>
      <DateInput />
      <Text
        style={{
          fontSize: 16,
          fontWeight: "bold",
          marginHorizontal: -3,
        }}
      >
        To
      </Text>
      <DateInput />
    </View>
  );
};

const styles = StyleSheet.create({
  inputBlock: {
    alignItems: "center",
    paddingVertical: 10,
    paddingHorizontal: 4,
    // backgroundColor: "red",
    marginTop: 35,
    justifyContent: "space-evenly",
    flexDirection: "row",
    marginTop: 12,
    padding: 3,
  },
});

export default ItenaryDates;
