import React, { useState } from "react";
import {
  View,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Text,
} from "react-native";
// import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import DateTimePicker from "@react-native-community/datetimepicker";
import { MaterialIcons } from "@expo/vector-icons";
const DateField = () => {
  const [date, setDate] = useState(new Date(1598051730000));
  const [mode, setMode] = useState("date");
  const [show, setShow] = useState(false);

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShow(Platform.OS === "ios");
    setDate(currentDate);
  };

  const showDatepicker = () => {
    console.log("clicked");
    setShow(true);
  };
  return (
    <View style={styles.inputBlock}>
      <View style={{ flexDirection: "row", justifyContent: "center" }}>
        <TextInput
          editable={false}
          value={`${date.toLocaleDateString()}`}
          style={styles.input}
          placeholder="DD/MM/YY"
        />
        <TouchableOpacity style={styles.date_icon} onPress={showDatepicker}>
          <MaterialIcons name="date-range" size={24} color="#5864D2" />
          {/* <Text>Date</Text> */}
        </TouchableOpacity>
      </View>
      {show && (
        <DateTimePicker
          testID="dateTimePicker"
          value={date}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  inputBlock: {
    alignItems: "center",
    padding: 3,
  },
  date_icon: {
    position: "absolute",
    top: 8,
    left: 0,
    // zIndex: 3,
  },
  input: {
    paddingVertical: 5,
    width: "59%",
    paddingLeft: 26,
    borderBottomWidth: 2,
  },
});

export default DateField;
