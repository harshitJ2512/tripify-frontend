import React, { useState } from "react";
import { Text, View, StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";
import PhoneInput from "react-native-phone-number-input";
import ActionButton from "../../components/ActionButton";
import { loginHeadings } from "../../styles/typography";
import { colors } from "../../styles/colors";

const PhoneNumberBox = () => {
  const [countryCode, setCountryCode] = useState("+91");
  const [number, setNumber] = useState("");
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <Text style={[loginHeadings, { marginTop: 20 }]}>
        Enter Your Mobile Number
      </Text>
      <PhoneInput
        defaultCode="IN"
        layout="first"
        value={number}
        onChangeText={(text) => {
          setNumber(text);
        }}
        onChangeCountry={(country) => {
          setCountryCode(`+${country?.callingCode[0]}`);
        }}
        autoFocus
        containerStyle={{
          width: "100%",
          borderWidth: 1,
          borderColor: colors.border,
          borderRadius: 5,
          marginTop: 20,
          shadowColor: "#333333",
          shadowOffset: {
            width: 0,
            height: 0.5,
          },
          shadowOpacity: 0.16,
          shadowRadius: 30,
        }}
      />
      <ActionButton
        loading={false}
        text="Get OTP"
        onClick={() => navigation.navigate("otp")}
        customStyle={{ marginTop: 30 }}
      />
    </View>

    //
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    position: "absolute",
    height: 270,
    bottom: 0,
    left: 0,
    right: 0,
    paddingHorizontal: 16,
    shadowColor: "#333333",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.16,
    shadowRadius: 30,
  },

  text: {
    fontSize: 19,
    fontWeight: "900",
    color: "#000",
  },
});

export default PhoneNumberBox;
