import React from "react";
import { ScrollView } from "react-native";
import { colors } from "../../styles/colors";
import Ongoing from "./Ongoing";
import Past from "./Past";
import PersonalView from "./PersonalView";
import Stats from "./Stats";
import Upcoming from "./Upcoming";

const Dashboard = () => {
  return (
    <ScrollView
      style={{ backgroundColor: colors.screenbg }}
      contentContainerStyle={{ paddingBottom: 130 }}
    >
      <PersonalView />
      <Stats />
      <Ongoing />
      <Upcoming />
      <Past />
    </ScrollView>
  );
};

export default Dashboard;
