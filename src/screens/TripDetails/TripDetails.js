import React from "react";
import {
  Image,
  SafeAreaView,
  StyleSheet,
  View,
  Text,
  ScrollView,
  Platform,
  StatusBar,
  TouchableWithoutFeedback,
} from "react-native";
import { loginHeadings } from "../../styles/typography";
import Mountain from "../../assets/Mountain.jpg";
import { colors } from "../../styles/colors";
import { Ionicons } from "@expo/vector-icons";
import PayButton from "../../components/PayButton";

const TripDetails = ({ navigation }) => {
  return (
    <SafeAreaView
      style={{
        backgroundColor: colors.screenbg,
        flex: 1,
        // paddingTop: Platform.OS == "ios" ? 0 : 10,
      }}
    >
      <StatusBar style="dark" />
      <View style={styles.topBar}>
        <TouchableWithoutFeedback onPress={() => navigation.goBack()}>
          <Ionicons name="arrow-back" size={34} color={colors.greyText} />
        </TouchableWithoutFeedback>
      </View>
      <ScrollView
        contentContainerStyle={{ paddingHorizontal: 16, paddingBottom: 80 }}
      >
        <View style={styles.imgContainer}>
          <Image source={Mountain} style={styles.img} />
        </View>
        <Text style={styles.header}>Your trip to Manali</Text>
        <Text style={styles.status}>Ongoing</Text>
        <View style={styles.itineraryContainer}>
          <View style={styles.leftContainer}>
            <Text style={styles.label}>Starts on</Text>
            <Text style={styles.value}>Tue, 23rd July, 2021</Text>
            <Text style={styles.value}>12:00 am</Text>
          </View>
          <View style={styles.rightContainer}>
            <Text style={styles.label}>Ends on</Text>
            <Text style={styles.value}>Tue, 23rd July, 2021</Text>
            <Text style={styles.value}>12:00 am</Text>
          </View>
        </View>

        <Text style={styles.subheader}>Trip Details</Text>
        <View style={styles.detail}>
          <Text style={styles.label}>Members</Text>
          <Text style={styles.data}>4</Text>
        </View>

        <View style={styles.detail}>
          <Text style={styles.label}>Trip id</Text>
          <Text style={styles.data}>#2309AB67</Text>
        </View>

        <View style={styles.detail}>
          <Text style={styles.label}>Total amount available</Text>
          <Text style={styles.data}>₹2500</Text>
        </View>

        <View style={styles.detail}>
          <Text style={styles.label}>Total amount pooled</Text>
          <Text style={styles.data}>₹5000</Text>
        </View>
      </ScrollView>
      <PayButton />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  topBar: {
    height: 50,
    borderBottomColor: colors.border,
    borderBottomWidth: 1,
    paddingHorizontal: 16,
    justifyContent: "center",
  },
  header: {
    ...loginHeadings,
    fontSize: 35,
    marginTop: 20,
  },
  status: {
    color: "#43a047",
    fontFamily: "Montserrat_600SemiBold",
    fontSize: 16,
    marginLeft: 4,
    marginTop: 4,
  },
  subheader: {
    ...loginHeadings,
    fontSize: 22,
    marginTop: 20,
  },
  itineraryContainer: {
    flexDirection: "row",
    marginTop: 20,
    borderBottomColor: colors.border,
    borderBottomWidth: 1,
    paddingVertical: 20,
    borderTopColor: colors.border,
    borderTopWidth: 1,
  },
  imgContainer: {
    width: "100%",
    borderRadius: 16,
    height: 200,
    marginTop: 20,
  },
  img: {
    borderRadius: 16,
    height: 200,
    width: "100%",
  },
  leftContainer: {
    width: "50%",
    borderRightWidth: 1,
    borderRightColor: colors.border,
  },
  rightContainer: {
    width: "50%",
    marginLeft: 20,
  },
  label: {
    ...loginHeadings,
    fontSize: 15,
  },
  value: {
    marginTop: 5,
    fontSize: 16,
    color: colors.primaryText,
    fontFamily: "Montserrat_400Regular",
  },
  detail: {
    paddingVertical: 18,
    borderBottomColor: colors.border,
    borderBottomWidth: 1,
  },
  data: {
    marginTop: 10,
    fontSize: 16,
    color: colors.primaryText,
    fontFamily: "Montserrat_400Regular",
  },
});

export default TripDetails;
