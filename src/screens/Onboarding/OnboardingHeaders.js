import React from "react";
import { View, Text, StyleSheet, SafeAreaView } from "react-native";
import { colors } from "../../styles/colors";

const Onboarding1 = ({ text }) => {
  return (
    <View>
      <Text style={styles.head1}>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  head1: {
    color: colors.primaryText,
    fontSize: 44,
    fontFamily: "Montserrat_700Bold",
    paddingHorizontal: 16,
    marginTop: 20,
  },
});

export default Onboarding1;
