import React, { useEffect, useRef } from "react";
import {
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  View,
} from "react-native";
import { colors } from "../../styles/colors";
import OnboardingHeaders from "../Onboarding/OnboardingHeaders";
import { btnText, mediumParagraph } from "../../styles/typography";
import { onbBtn } from "../../styles/buttons";
import LottieView from "lottie-react-native";
import Dimensions from "../../utils/Dimensions";
import { useNavigation } from "@react-navigation/native";

const Onboarding3 = () => {
  const animation = useRef(null);
  const navigation = useNavigation();

  useEffect(() => {
    if (animation.current) {
      animation.current.play();
    }
  }, []);

  return (
    <SafeAreaView style={styles.bg}>
      <View style={{ marginBottom: 110 }}>
        <LottieView
          ref={animation}
          autoPlay
          loop
          style={{
            height: 300,
            backgroundColor: colors.ob3,
            width: Dimensions.width,
            marginBottom: 20,
          }}
          source={require("../../assets/ob3.json")}
        />
        <OnboardingHeaders text="Let's get started" />
      </View>
      <TouchableOpacity
        style={styles?.btn}
        onPress={() => {
          navigation.push("login");
        }}
      >
        <Text style={btnText}>Next</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  bg: {
    backgroundColor: colors?.ob3,
    flex: 1,
    fontWeight: "800",
    flexDirection: "column-reverse",
  },
  tagLine: {
    ...mediumParagraph,
    marginHorizontal: 16,
  },
  btn: {
    ...onbBtn,
    backgroundColor: colors?.ob3Btn,
    position: "absolute",
    width: "90%",
    bottom: 30,
  },
});

export default Onboarding3;
