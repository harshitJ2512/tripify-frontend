import React, { useEffect, useRef } from "react";
import { Text, StyleSheet, SafeAreaView, TouchableOpacity } from "react-native";
import { colors } from "../../styles/colors";
import OnboardingHeaders from "../Onboarding/OnboardingHeaders";
import LottieView from "lottie-react-native";
import { btnText, mediumParagraph } from "../../styles/typography";
import { onbBtn } from "../../styles/buttons";
import { useNavigation } from "@react-navigation/native";

const Onboarding1 = () => {
  const animation = useRef(null);
  const navigation = useNavigation();

  useEffect(() => {
    animation.current.play();
  }, []);

  return (
    <SafeAreaView style={styles.bg}>
      <OnboardingHeaders text="Reimagine your Expenses" />
      <LottieView
        ref={animation}
        autoPlay
        loop
        style={{
          width: 400,
          height: 400,
          backgroundColor: colors.ob1,
        }}
        source={require("../../assets/ob1.json")}
      />
      <Text style={styles.tagLine}>
        Everything you need during your adventures all in one place
      </Text>
      <TouchableOpacity
        style={styles.btn}
        onPress={() => {
          navigation.push("onb2");
        }}
      >
        <Text style={btnText}>Next</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  bg: {
    backgroundColor: colors.ob1,
    flex: 1,
    fontWeight: "800",
  },
  tagLine: {
    ...mediumParagraph,
    marginHorizontal: 16,
  },
  btn: {
    ...onbBtn,
    backgroundColor: colors.ob1Btn,
    position: "absolute",
    width: "90%",
    bottom: 30,
  },
});

export default Onboarding1;
