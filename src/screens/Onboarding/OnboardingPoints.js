import React from "react";
import { Text, View, StyleSheet } from "react-native";
import { mediumParagraph } from "../../styles/typography";
import { Ionicons } from "@expo/vector-icons";

const OnboardingPoints = ({ text }) => {
  return (
    <View style={styles.container}>
      <Ionicons name="ios-checkmark-circle" size={24} color="#333333" />
      <Text style={styles.points}>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: 15,
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: 16,
    marginTop: 10,
  },
  points: {
    ...mediumParagraph,
    fontSize: 16,
    marginLeft: 5,
  },
});

export default OnboardingPoints;
