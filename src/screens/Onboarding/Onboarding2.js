import React, { useRef, useEffect } from "react";
import { Text, StyleSheet, SafeAreaView, TouchableOpacity } from "react-native";
import { colors } from "../../styles/colors";
import OnboardingHeaders from "../Onboarding/OnboardingHeaders";
import { btnText, mediumParagraph } from "../../styles/typography";
import { onbBtn } from "../../styles/buttons";
import OnboardingPoints from "./OnboardingPoints";
import { useNavigation } from "@react-navigation/native";
import LottieView from "lottie-react-native";
import Dimensions from "../../utils/Dimensions";

const Onboarding2 = () => {
  const navigation = useNavigation();
  const animation = useRef(null);

  useEffect(() => {
    animation.current.play();
  }, []);

  return (
    <SafeAreaView style={styles.bg}>
      <OnboardingHeaders text="What we Offer?" />
      <LottieView
        ref={animation}
        autoPlay
        loop
        style={{
          width: 400,
          height: 250,
          backgroundColor: colors.ob2,
          alignSelf: "center",
        }}
        source={require("../../assets/offer.json")}
      />
      <OnboardingPoints text="Fair finances during your trips" />
      <OnboardingPoints text="The security of UPI" />
      <OnboardingPoints text="Payment privileges for every member" />
      <OnboardingPoints text="Rewards on payment" />
      <OnboardingPoints text="Trip management" />
      <TouchableOpacity
        style={styles.btn}
        onPress={() => {
          navigation.push("onb3");
        }}
      >
        <Text style={btnText}>Next</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  bg: {
    backgroundColor: colors?.ob2,
    flex: 1,
    fontWeight: "800",
  },
  tagLine: {
    ...mediumParagraph,
    marginHorizontal: 16,
  },
  btn: {
    ...onbBtn,
    backgroundColor: colors.ob2Btn,
    position: "absolute",
    width: "90%",
    bottom: 30,
  },
});

export default Onboarding2;
