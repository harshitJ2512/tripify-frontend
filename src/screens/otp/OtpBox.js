import React, { useRef, useState, useContext } from "react";
import { Text, View, StyleSheet, KeyboardAvoidingView } from "react-native";
import OTPTextInput from "react-native-otp-textinput";
import ActionButton from "../../components/ActionButton";
import { userContext } from "../../context/user";
import { colors } from "../../styles/colors";
import { loginHeadings } from "../../styles/typography";

const OtpBox = () => {
  const [disabled, setDisabled] = useState(true);
  const otpRef = useRef(null);
  const { login } = useContext(userContext);

  return (
    <View style={styles.container}>
      <Text style={[loginHeadings, { marginTop: 20 }]}>Enter the OTP</Text>
      <OTPTextInput
        ref={otpRef}
        inputCount={5}
        tintColor={colors.greyText}
        offTintColor={colors.border}
        handleTextChange={(code) => {
          if (code.length < 5) {
            if (!disabled) setDisabled(true);
          } else setDisabled(false);
        }}
        containerStyle={{
          marginTop: 20,
        }}
      />

      <ActionButton
        loading={false}
        text="Login"
        onClick={() => login()}
        disabled={disabled}
        customStyle={{
          marginTop: 30,
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "#fff",
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    position: "absolute",
    height: 270,
    bottom: 0,
    left: 0,
    right: 0,
    paddingHorizontal: 16,
    shadowColor: "#333333",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.16,
    shadowRadius: 30,
    zIndex: 3,
  },

  text: {
    fontSize: 19,
    fontWeight: "900",
    color: "#000",
  },
  borderStyleHighLighted: {
    borderColor: "#03DAC6",
  },
});

export default OtpBox;
