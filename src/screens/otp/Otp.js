import React, { useEffect, useRef, useState } from "react";
import {
  View,
  StyleSheet,
  Image,
  Platform,
  StatusBar,
  KeyboardAvoidingView,
} from "react-native";
import Carousel from "react-native-snap-carousel";
import WithTimer from "../../components/WithTimer";
import OtpBox from "./OtpBox";
import travel1 from "../../assets/travel1.png";
import travel2 from "../../assets/travel2.jpg";
import travel3 from "../../assets/travel3.jpg";
import Dimensions from "../../utils/Dimensions";

const Otp = () => {
  const arr = [travel1, travel2, travel3];
  let carouselRef = useRef();

  const renderElement = ({ item, index }) => {
    return (
      <Image
        style={{
          height: Dimensions.height - 100,
          width: Dimensions.width,
          resizeMode: "cover",
        }}
        source={arr[index]}
      />
    );
  };

  return (
    <View style={styles.container}>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
        style={{ height: "100%" }}
      >
        <WithTimer ref={carouselRef} timer={6000}>
          <Carousel
            layout="default"
            ref={carouselRef}
            data={arr}
            renderItem={renderElement}
            sliderWidth={Dimensions.width}
            itemWidth={Dimensions.width}
            slideStyle={{ width: Dimensions.width }}
            scrollEnabled={false}
          />
        </WithTimer>
        <OtpBox />
      </KeyboardAvoidingView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    width: "100%",
    height: "100%",
  },
});

export default Otp;
