import React from "react";
import { Image, StyleSheet, Text, View } from "react-native";
import { colors } from "../../styles/colors";
import { header, mediumParagraph } from "../../styles/typography";
import Avatar from "../../assets/avatar.jpeg";

const PersonalView = () => {
  return (
    <View style={styles.bg}>
      <Image source={Avatar} style={styles.avatar} />
      <View style={styles.text}>
        <Text style={header}>Hi, Karan</Text>
        <Text style={styles.greet}>Glad to see you again!</Text>
      </View>
    </View>
  );
};

export default PersonalView;

const styles = StyleSheet.create({
  bg: {
    backgroundColor: "#333B6E",
    height: 180,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 16,
    // borderBottomLeftRadius: 16,
    // borderBottomRightRadius: 16,
  },
  text: {
    marginLeft: 20,
    marginTop: 20,
  },
  greet: {
    ...mediumParagraph,
    color: colors.screenbg,
    fontSize: 18,
  },
  avatar: {
    height: 80,
    width: 80,
    borderRadius: 40,
    backgroundColor: colors.screenbg,
    marginTop: 20,
  },
});
