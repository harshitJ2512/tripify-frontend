import React from "react";
import { StyleSheet, View, Text, Image, ScrollView } from "react-native";
import TripTile from "../../components/TripTile";
import { colors } from "../../styles/colors";
import { subtitle } from "../../styles/typography";
import Jaipur from "../../assets/Jaipur.jpeg";
import Delhi from "../../assets/Delhi.jpeg";
import Goa from "../../assets/Goa.jpeg";
import Mumbai from "../../assets/Mumbai.jpeg";

const upcomingTrips = [
  { title: "Jaipur", img: Jaipur, sub: "Starts in 5 days" },
  { title: "Delhi", img: Delhi, sub: "Starts in 20 days" },
  { title: "Goa", img: Goa, sub: "Starts in 60 days" },
  { title: "Mumbai", img: Mumbai, sub: "Starts in 90 days" },
];

const Upcoming = () => {
  return (
    <View style={styles.ongoing}>
      <Text style={[subtitle, { marginLeft: 16 }]}>Upcoming Trips</Text>
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{ paddingHorizontal: 16 }}
        style={{ marginTop: 10 }}
      >
        {upcomingTrips.map((item) => (
          <TripTile img={item.img} title={item.title} sub={item.sub} />
        ))}
      </ScrollView>
    </View>
  );
};

export default Upcoming;

const styles = StyleSheet.create({
  ongoing: {
    marginTop: 20,
  },
  tripImage: {
    width: "30%",
    height: 129,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
  },
  info: {
    marginLeft: 10,
    marginTop: 5,
    color: colors.primaryText,
  },
  infoHeader: {
    fontSize: 18,
    fontFamily: "Montserrat_500Medium",
    marginBottom: 5,
  },
  infoData: {
    fontSize: 14,
    fontFamily: "Montserrat_400Regular",
    color: colors.greyText,
  },
  container: {
    height: 130,
    borderRadius: 8,
    marginTop: 10,
    borderWidth: 1,
    borderColor: colors.border,
    flexDirection: "row",
  },
});
