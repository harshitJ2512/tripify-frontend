import React from "react";
import { StyleSheet, View } from "react-native";
import StatsTile from "../../components/StatsTile";
import { colors } from "../../styles/colors";
import destination from "../../assets/destination.png";
import paisa from "../../assets/paisa.png";
const Stats = () => {
  return (
    <View style={styles.container}>
      <StatsTile
        icon={destination}
        number={3}
        desc="Trips Taken"
        primary="#BAB6FF"
        secondary="#F6F8FE"
      />
      <StatsTile
        icon={paisa}
        number="₹5000"
        desc="Total Spent"
        primary="#6BCA9A"
        secondary="#EFFCF4"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginHorizontal: 16,
    marginTop: 20,
  },
});

export default Stats;
