import React from "react";
import { StyleSheet, View, Text, Image, ScrollView } from "react-native";
import TripTile from "../../components/TripTile";
import { colors } from "../../styles/colors";
import { subtitle } from "../../styles/typography";
import SFO from "../../assets/SFO.jpeg";
import Kerala from "../../assets/Kerala.jpeg";
import London from "../../assets/London.jpeg";

const pastTrips = [
  { title: "SFO", img: SFO, sub: "Ended 9 days ago" },
  { title: "Kerala", img: Kerala, sub: "Ended 30 days ago" },
  { title: "London", img: London, sub: "Ended 60 days ago" },
];

const Past = () => {
  return (
    <View style={styles.ongoing}>
      <Text style={[subtitle, { marginLeft: 16 }]}>Past Trips</Text>
      <ScrollView
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={{ paddingHorizontal: 16 }}
        style={{ marginTop: 10 }}
      >
        {pastTrips.map((item) => (
          <TripTile img={item.img} title={item.title} sub={item.sub} />
        ))}
      </ScrollView>
    </View>
  );
};

export default Past;

const styles = StyleSheet.create({
  ongoing: {
    marginTop: 20,
  },
  tripImage: {
    width: "30%",
    height: 129,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
  },
  info: {
    marginLeft: 10,
    marginTop: 5,
    color: colors.primaryText,
  },
  infoHeader: {
    fontSize: 18,
    fontFamily: "Montserrat_500Medium",
    marginBottom: 5,
  },
  infoData: {
    fontSize: 14,
    fontFamily: "Montserrat_400Regular",
    color: colors.greyText,
  },
  container: {
    height: 130,
    borderRadius: 8,
    marginTop: 10,
    borderWidth: 1,
    borderColor: colors.border,
    flexDirection: "row",
  },
});
