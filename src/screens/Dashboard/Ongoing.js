import React from "react";
import {
  StyleSheet,
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  TouchableOpacity,
} from "react-native";
import { colors } from "../../styles/colors";
import { subtitle } from "../../styles/typography";
import Mountain from "../../assets/Mountain.jpg";
import { useNavigation } from "@react-navigation/native";

const Ongoing = () => {
  const navigation = useNavigation();

  return (
    <TouchableOpacity
      style={styles.ongoing}
      onPress={() => navigation.navigate("Details")}
    >
      <Text style={subtitle}>Ongoing Trip</Text>
      <View style={styles.container}>
        <Image source={Mountain} style={styles.tripImage} />
        <View style={styles.info}>
          <Text style={styles.infoHeader}>Trip to Manali</Text>
          <Text style={styles.infoData}>Members: 5-6</Text>
          <Text style={styles.infoData}>Min. Pool Amount: ₹5000</Text>
          <Text style={styles.infoData}>Total Amount: ₹25000</Text>
          <Text style={styles.infoData}>Itinerary: 26th Aug - 31st Sept</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Ongoing;

const styles = StyleSheet.create({
  ongoing: {
    margin: 16,
    marginTop: 20,
  },
  tripImage: {
    width: "30%",
    height: 129,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
  },
  info: {
    marginLeft: 10,
    marginTop: 5,
    color: colors.primaryText,
  },
  infoHeader: {
    fontSize: 18,
    fontFamily: "Montserrat_500Medium",
    marginBottom: 5,
  },
  infoData: {
    fontSize: 14,
    fontFamily: "Montserrat_400Regular",
    color: colors.greyText,
  },
  container: {
    height: 130,
    borderRadius: 8,
    marginTop: 10,
    borderWidth: 1,
    borderColor: colors.border,
    flexDirection: "row",
  },
});
