import React, { useContext } from "react";
import {
  Button,
  SafeAreaView,
  StyleSheet,
  Platform,
  StatusBar,
} from "react-native";
import { userContext } from "../../context/user";

const Settings = () => {
  const { logout } = useContext(userContext);
  return (
    <SafeAreaView style={styles.container}>
      <Button title="Logout" onPress={logout} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingTop: Platform.OS == "ios" ? 0 : StatusBar.currentHeight,
  },
});

export default Settings;
