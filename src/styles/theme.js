import { extendTheme } from "native-base";

export const theme = extendTheme({
  components: {
    Select: {
      variants: {
        plain_select: ({ colorScheme }) => {
          return {
            bg: `red`,
            // rounded: "full",
            borderWidth: 2,
            borderColor: "#333",
          };
        },
      },
    },
  },
});
