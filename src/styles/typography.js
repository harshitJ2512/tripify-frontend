import { colors } from "./colors";

export const btnText = {
  color: colors.btnText,
  fontSize: 24,
  fontFamily: "Montserrat_600SemiBold",
};

export const smallParagraph = {
  color: colors.greyText,
  fontSize: 12,
  fontWeight: "400",
  fontFamily: "Montserrat",
};

export const mediumParagraph = {
  color: colors.greyText,
  fontSize: 14,
  fontWeight: "400",
  fontFamily: "Montserrat_500Medium",
};

export const header = {
  color: colors.screenbg,
  fontSize: 35,
  fontWeight: "700",
  fontFamily: "Montserrat_700Bold",
};

export const subtitle = {
  color: colors.greyText,
  fontSize: 16,
  fontWeight: "500",
  fontFamily: "Montserrat_500Medium",
};

export const loginHeadings = {
  color: colors.primaryText,
  fontSize: 24,
  fontFamily: "Montserrat_600SemiBold",
};
