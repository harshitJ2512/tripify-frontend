export const colors = {
  primaryText: "#333333",
  primaryBtn: "#5864D2",
  btnText: "white",
  screenbg: "white",
  ob1: "#E6F2FC",
  ob1Btn: "#335C9C",
  ob2: "#DFE2F8",
  ob2Btn: "#7077A1",
  ob3: "#F9F5FF",
  ob3Btn: "#746392",
  border: "#ebebeb",
  greyText: "#888888",
};
