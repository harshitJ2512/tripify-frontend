import React, { createContext, useState } from "react";

export const userContext = createContext();

const UserProvider = (props) => {
  const [authenticated, setAuthenticated] = useState(false);

  const login = () => {
    setAuthenticated(true);
  };

  const logout = () => {
    setAuthenticated(false);
  };

  return (
    <userContext.Provider value={{ authenticated, login, logout }}>
      {props.children}
    </userContext.Provider>
  );
};

export default UserProvider;
