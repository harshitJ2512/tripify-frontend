import React from "react";
import { StyleSheet, TouchableOpacity, Text } from "react-native";
import { colors } from "../styles/colors";
import { btnText } from "../styles/typography";

const PayButton = () => {
  return (
    <TouchableOpacity style={styles.btn}>
      <Text style={btnText}>Make Payment</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btn: {
    position: "absolute",
    bottom: 40,
    backgroundColor: colors.primaryBtn,
    height: 60,
    width: 230,
    padding: 10,
    borderRadius: 22,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
  },
});

export default PayButton;
