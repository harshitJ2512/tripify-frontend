import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import { colors } from "../styles/colors";
import { MaterialIcons } from "@expo/vector-icons";
import { mediumParagraph } from "../styles/typography";

const TripTile = ({ title, img, sub }) => {
  return (
    <View style={styles.container}>
      <Image source={img} style={styles.img} />
      <View style={styles.info}>
        <Text style={styles.title}>Trip to {title}</Text>
        <Text style={styles.starts}>{sub}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: 300,
    width: 280,
    borderRadius: 16,
    borderWidth: 1,
    borderColor: colors.border,
    marginRight: 10,
  },
  info: {
    padding: 16,
  },
  title: {
    color: colors.primaryText,
    fontSize: 26,
    fontFamily: "Montserrat_600SemiBold",
  },
  img: {
    height: 210,
    width: 279,
    borderTopLeftRadius: 16,
    borderTopRightRadius: 16,
    resizeMode: "cover",
  },
  starts: {
    ...mediumParagraph,
    marginTop: 5,
  },
});

export default TripTile;
