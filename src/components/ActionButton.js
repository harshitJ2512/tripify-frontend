import React from "react";
import {
  Text,
  StyleSheet,
  ActivityIndicator,
  View,
  // TouchableOpacity,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { colors } from "../styles/colors";

const ActionButton = ({ loading, text, customStyle, onClick, disabled }) => {
  console.log(disabled, onClick);
  return (
    <TouchableOpacity onPress={disabled ? null : () => onClick()}>
      <View
        style={[
          styles.container,
          { opacity: disabled ? 0.5 : 1 },
          { ...customStyle },
        ]}
      >
        {loading ? (
          <ActivityIndicator size="large" color="#fff" />
        ) : (
          <Text style={styles.text}>{text}</Text>
        )}
      </View>
    </TouchableOpacity>
  );
};
export default ActionButton;
const styles = StyleSheet.create({
  container: {
    justifyContent: "center",
    alignItems: "center",
    height: 59,
    borderRadius: 5,
    backgroundColor: colors.primaryBtn,
    width: "100%",
  },
  text: {
    color: colors.btnText,
    fontSize: 24,
    fontWeight: "500",
  },
});
