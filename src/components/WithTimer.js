import React, { forwardRef, useEffect } from "react";

const WithTimer = (props, ref) => {
  // useEffect(() => {
  //   const interval = setInterval(() => {
  //     const idx = ref.current.currentIndex;
  //     console.log(idx);
  //     if (idx < 2) {
  //       ref.current.snapToNext();
  //     } else ref.current.snapToItem(0);
  //   }, props.timer);
  //   () => {
  //     clearInterval(interval);
  //   };
  // }, []);
  return props.children;
};

export default forwardRef(WithTimer);
