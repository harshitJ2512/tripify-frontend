import React from "react";
import { StyleSheet, View, Text, Image } from "react-native";
import { colors } from "../styles/colors";

const StatsTile = ({ icon, number, desc, primary, secondary }) => {
  const styles = StyleSheet.create({
    container: {
      height: 140,
      width: "48%",
      borderWidth: 1,
      borderColor: colors.border,
      borderRadius: 16,
      padding: 10,
      backgroundColor: secondary,
    },
    number: {
      color: colors.primaryText,
      fontSize: 40,
      fontFamily: "Montserrat_600SemiBold",
    },
    desc: {
      color: "#9097AC",
      fontSize: 20,
      fontFamily: "Montserrat_500Medium",
    },
    iconContainer: {
      borderRadius: 8,
      backgroundColor: primary,
      justifyContent: "center",
      alignItems: "center",
      height: 35,
      width: 35,
    },
    icon: {
      height: 25,
      width: 25,
    },
  });
  return (
    <View style={styles.container}>
      <View style={styles.iconContainer}>
        <Image source={icon} style={styles.icon} />
      </View>
      <Text style={styles.number}>{number}</Text>
      <Text style={styles.desc}>{desc}</Text>
    </View>
  );
};

export default StatsTile;
